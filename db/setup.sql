-- Creazione account admin

INSERT INTO utente (Nome, Cognome, Email, Password)
VALUES ("admin", "admin", "admin@tecweb.it", "c68316cad5a6af703c89b0290ca790e6b135151b4c2c6dfdcebb1ec3f981f854");


-- Setup articoli

INSERT INTO articolo
VALUES (1, "Accendini", "Accessori", 1, 10);

INSERT INTO articolo
VALUES (2, "CD - Stai Ascoltando l'Indecenza", "Musica", 10, 223);

INSERT INTO articolo
VALUES (3, "Maglietta basic", "Abbigliamento", 10, 15);

INSERT INTO articolo
VALUES (4, "Maglietta backlogo", "Abbigliamento", 15, 5);


-- Setup concerti (only for debug)

INSERT INTO articolo
VALUES (5, "Concerto Forlimpopoli", "Biglietti concerti", 20, 200);

INSERT INTO concerto
VALUES (5, "Concerto Forlimpopoli", "Forlimpopoli", STR_TO_DATE('12-01-2021', '%d-%m-%Y'), 20);

INSERT INTO articolo
VALUES (6, "Concerto Wembley", "Biglietti concerti", 30, 90000);

INSERT INTO concerto
VALUES (6, "Concerto Wembley", "Wembley Stadium - London", STR_TO_DATE('25-12-2020', '%d-%m-%Y'), 19);