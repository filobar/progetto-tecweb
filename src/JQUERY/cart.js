$(document).ready(function(){

    $(".increment").click(function(e){
        var xhttp;
        xhttp = new XMLHttpRequest();

        var buttonClicked = e.target;
        $(buttonClicked).prop("disabled", true);
        $(buttonClicked).next().prop("disabled", true);

        // QUI prendo l'id dell'oggetto
        var id = $(buttonClicked).parent().parent().children("p").first().prop("id").split("quantity_")[1];

        // QUI prendo la quantità dell'oggetto
        var quantity = parseInt($(buttonClicked).parent().parent().children("p").first().html().split(":")[1]);

        // QUI prendo il totale dell'oggetto
        var total = parseFloat($("#total_" + id).html().split("€")[1]);
        total = total / quantity * (quantity+1);

        quantity = quantity + 1;

        // Qui aggiorno il totale dell'ordine
        var total_order = parseFloat($("#total_order").html().split("€")[1]);
        $("#total_order").html("Totale ordine: €" + (total_order+(total/quantity)).toFixed(2));    

        xhttp.onreadystatechange = function() {
            var element = $(buttonClicked).parent().parent().children("p").first();

            if (this.readyState == 4 && this.status == 200) {

                // Aggiorno il totale dell'oggetto
                $("#total_" + id).html("€" + total.toFixed(2));

                var value = parseInt(element.html().split(":")[1])+1;
                element.html(element.html().split(":")[0] + ": " + value);
                $(buttonClicked).prop("disabled", false);
                $(buttonClicked).next().prop("disabled", false);
            }
        };

        xhttp.open("GET", "../PHP/update.php?id=" + id + "&q=" + quantity, true);
        xhttp.send();
    });

    $(".decrement").click(function(e){
        var xhttp;
        xhttp = new XMLHttpRequest();

        var buttonClicked = e.target;
        $(buttonClicked).prop("disabled", true);
        $(buttonClicked).next().prop("disabled", true);

        // QUI prendo l'id
        var id = $(buttonClicked).parent().parent().children("p").first().prop("id").split("quantity_")[1];

        // QUI prendo la quantità
        var quantity = parseInt($(buttonClicked).parent().parent().children("p").first().html().split(":")[1]);

        // QUI prendo il totale dell'oggetto
        var total = parseFloat($("#total_" + id).html().split("€")[1]);

        var unitPrice = total/quantity;

        if (quantity-1 == 0) {
            total = 0;

            // Nascondo l'oggetto dalla pagina
            $(buttonClicked).parent().parent().parent().hide();
        }
        else total = unitPrice * (quantity-1);

        quantity = quantity - 1;
        
        // Qui aggiorno il totale dell'ordine
        var total_order = parseFloat($("#total_order").html().split("€")[1]);
        $("#total_order").html("Totale ordine: €" + (total_order-unitPrice).toFixed(2));

        xhttp.onreadystatechange = function() {
            var element = $(buttonClicked).parent().parent().children("p").first();

            if (this.readyState == 4 && this.status == 200) {

                // Aggiorno il totale dell'oggetto
                $("#total_" + id).html("€" + total.toFixed(2));

                var value = parseInt(element.html().split(":")[1])-1;
                element.html(element.html().split(":")[0] + ": " + value);
                $(buttonClicked).prop("disabled", false);
                $(buttonClicked).next().prop("disabled", false);
            }
        };

        xhttp.open("GET", "../PHP/update.php?id=" + id + "&q=" + quantity, true);
        xhttp.send();
    });

    $("#empty_cart").click(function(){
        console.log("click");
        var xhttp;
        xhttp = new XMLHttpRequest();

        xhttp.open("GET", "../PHP/update.php?id=-1", true);
        xhttp.send();

        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                window.location.href = "cart_index.php";
            }
        };        
    });

    $("#go_shop").click(function(e){
        window.location.href = "shop_index.php";
    });

    $("#complete_order").click(function(){
        window.location.href = "complete_order_index.php";
    });

});