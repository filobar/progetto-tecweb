$(document).ready(function(){

	$(window).keydown(function(event){  // Disabilito invio, così l'utente deve premere per forza il bottone.
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
    });

	$("#btn_reg_log").click(function(){
        window.location.href = "../PHP/register_index.php";
	});

	$("#btn_login").click(function(e){
        e.preventDefault();
        $("#password").val(hashCode($("#password").val()));
        $("form#log_form").submit();
    });

});

function hashCode (str){        // Source: StackOverflow https://stackoverflow.com/questions/26057572/string-to-unique-hash-in-javascript-jquery
    str = String(str);
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}