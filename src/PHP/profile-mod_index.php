<?php
    require_once("bootstrap.php");

    $templateParams["titolo"] = "I Santini - Modifica profilo";
    $templateParams["page"] = "profile-mod.php";
    $templateParams["name"] = "profile-mod";

    require("template/base.php");
?>