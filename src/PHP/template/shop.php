<section id="selectors">
    <h2>Selettori</h2>
    <div id="d1">
        <label for="categories">Categoria: </label>
        <select name="selection" id="categories">
            <option value="Tutti">Tutti</option>
        <?php foreach ($dbh->getCategories() as $categoria): ?>
            <?php if ($categoria["Tipo"]!="Biglietti concerti"): ?>
                <option value="<?php echo $categoria["Tipo"] ?>"><?php echo ucfirst($categoria["Tipo"]) ?></option>
            <?php endif; ?>
        <?php endforeach;?>

        </select>
    </div>
    <div id="d2">
        <label for="sorting">Ordinamento: </label>
        <select name="selection" id="sorting">
            <option value="alf">Alfabetico</option>
            <option value="pr_asc">Prezzo ascendente</option>
            <option value="pr_desc">Prezzo discendente</option>
            <option value="disp">Più disponibili</option>
        </select>
    </div>
</section>

<?php
    if(empty($_GET["sort"])) $sort = "alf";
    else $sort = $_GET["sort"];
    $products = $dbh->getProducts($sort);
?>

<?php foreach ($products as $product):?>
    <?php if ($product["Tipo"]!="Biglietti concerti"): ?>
        <?php 
            if (isAdmin()): 
                echo "<div>";
            else:
                echo "<div id=\"".$product["ID_articolo"]."\">";
            endif;
        ?>
        
            <article class="<?php echo lcfirst($product["Tipo"]) ?>">
                <h3><?php echo $product["Nome"] ?></h3>
                <img src="../../img/shop/<?php echo $product["Tipo"] ?>/<?php echo str_replace(' ', '_', $product["Nome"]); ?>1.jpeg" alt="<?php echo $product["Nome"] ?>"/>
                <div>
                    <p>€<?php echo $product["Prezzo"] ?> al pezzo</p>
                    <?php if (isAdmin() || intval($product["Numero_rimasti"]) <= 10): ?>
                        <p>Rimanenti: <?php echo intval($product["Numero_rimasti"]); ?></p>
                    <?php endif; ?>
                </div>
                <?php if (isAdmin()): ?>
                    <p><button class="modify" id="modify_<?php echo $product["ID_articolo"]; ?>">Modifica articolo</button></p>
                    <p><button class="delete" id="delete_<?php echo $product["ID_articolo"]; ?>">Cancella articolo</button></p>
                <?php endif; ?>
            </article>
        </div>
        
    <?php endif; ?>
<?php endforeach;?>

<?php if(isAdmin()): ?>
    <form action="template/add_article.php" method="post" enctype="multipart/form-data">
        <table>
            <tbody>
                <tr>
                    <td><label for="name">Nome articolo: </label></td>
                    <td><input type="text" id="name" name="name" required /></td>
                </tr>
                <tr>
                    <td><label for="category">Categoria: </label></td>
                    <td><input type="text" id="category" name="category" required /></td>
                </tr>
                <tr>
                    <td><label for="price">Prezzo unitario: </label></td>
                    <td><input id="price" type="number" step="0.5" name="price" min="0.5" value="1" required /></td>
                </tr>
                <tr>
                    <td><label for="quantity">Disponibili: </label></td>
                    <td><input id="quantity" type="number" step="1" name="quantity" min="0" value="1" required /></td>
                </tr>
                <tr>
                    <td><label for="image">Immagine: </label></td>
                    <td><input id="image" type="file" name="image" required /></td>
                </tr>
            </tbody>
        </table>
        <input type="submit" value="Aggiungi articolo" />
    </form>
<?php endif; ?>