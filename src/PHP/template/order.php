<?php $total = 0; ?>
<?php foreach ($dbh->getOrder($_GET["id"]) as $articolo):?>
    <article>
        <h3>titolo</h3>
        <img src="../../img/shop/<?php echo $articolo["Tipo"] ?>/<?php echo str_replace(' ', '_', $articolo["Nome"]) ?>1.jpeg" alt="questo è un oggetto" />
        <div>
            <a href="product_index.php?id=<?php echo $articolo["ID_articolo"] ?>"><?php echo $articolo["Nome"] ?></a>
            <p id="quantity_<?php echo $articolo["ID_articolo"];?>">Quantità: <?php echo $articolo["Quantità"] ?></p>
            <p id="total_<?php echo $articolo["ID_articolo"];?>">€<?php 
                    $total = $total + number_format((float)($articolo["Prezzo"]*$articolo["Quantità"]), 2, '.', '');
                    echo number_format((float)($articolo["Prezzo"]*$articolo["Quantità"]), 2, '.', '');
                ?>
            </p>
        </div>
    </article>
<?php endforeach; ?>