<section>
    <h2>Registrazione</h2>
    <form method="post" action="register_index.php" id="reg_form">
        <p>
            <label for="name">Nome</label>
            <input type="text" name="name" id="name">
        </p>
        <p>
            <label for="surname">Cognome</label>
            <input type="text" name="surname" id="surname">
        </p>
        <p>
            <label for="email">Email</label>
            <input type="text" name="email" id="email">
        </p>
        <p>
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
        </p>
        <p>
            <label for="re-password">Ripeti password</label>
            <input type="password" name="re-password" id="re-password">
        </p>
        <p>
            <label for="city">*Città</label>
            <input type="text" name="city" id="city">
        </p>
        <p>
            <label for="country">*Paese</label>
            <input type="text" name="country" id="country">
        </p>
        <p>
            <label for="province">*Provincia</label>
            <input type="text" name="province" id="province">
        </p>
        <p>
            <label for="address">*Indirizzo</label>
            <input type="text" name="address" id="address">
        </p>
        <p id="tip">(I campi contrassegnati con * sono opzionali fino al primo acqusto)</p>
        <p>
            <button id="btn_register">Registrati</button>
        </p>
    </form>
</section>

<?php
    if( !empty($_POST["name"]) &&
        !empty($_POST["surname"]) &&
        !empty($_POST["email"]) &&
        !empty($_POST["password"]) &&
        !empty($_POST["re-password"]) &&
        strcmp($_POST["password"], $_POST["re-password"])==0):
        //var_dump($_POST["password"]);
        //var_dump(saltAndCryptPassword($_POST["password"]));
        if ($dbh->registerNewUser($_POST["name"],
                                  $_POST["surname"],
                                  $_POST["email"],
                                  saltAndCryptPassword($_POST["password"]),
                                  $_POST["country"],
                                  $_POST["province"],
                                  $_POST["city"],
                                  $_POST["address"]) == false) {
            echo "<p>Esiste già un account con questa e-mail</p>";
        }
        else {
            header("Location: login_index.php");
            die();
        }
    endif;
?>