<section>
    <h3>titolo</h3>
    <?php
        if(!isset($_COOKIE["night"])):
            echo "<img src=\"../../img/utils/miniaereologo.png\" alt=\"\" />";
        else:
            echo "<img src=\"../../img/utils/aereo-no-rainbow.png\" alt=\"\" />";
        endif;
    ?>
</section>
<?php foreach ($dbh->getHomeArticles() as $art):?>
    <article id=<?php echo $art["ID_articolo"]; ?> >
        <h2><?php echo $art["Titolo"]; ?></h2>
        <p><?php echo $art["Testo"]; ?></p>
        <?php if(isAdmin()): ?>
            <button class="modifica">Modifica</button>
            <button class="elimina">Elimina</button>
        <?php endif; ?>
    </article>
<?php endforeach; ?>

<?php if(isAdmin()): ?>
    <form action="updateHomeArticle.php" method="post">
        <table>
            <tbody>
                <tr>
                    <td><label for="title">Titolo: </label></td>
                    <td><input type="text" id="title" name="title" required /></td>
                </tr>
                <tr>
                    <td><label for="content">Testo: </label></td>
                    <td><input type="text" id="content" name="content" required /></td>
                </tr>
            </tbody>
        </table>
        <label for="query">query</label>
        <input id="query" type="text" name="query" value="add" required />
        <input id="addart" type="submit" value="Aggiungi articolo" />
    </form>
<?php endif; ?>