<form>
	<div>
		<label for="Numero">Numero della carta:</label>
		<input type="text" name="Numero" id="Numero">
		<br/>
		<label for="Nome">Nome sulla carta:</label>
		<input type="text" name="Nome" id="Nome">
		<br/>
		<label for="Scadenza-mese">Mese di scadenza:</label>
		<select name="Scadenza-mese" id="Scadenza-mese">
			<?php for ($i = 1; $i <= 12; $i++): ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php endfor ?>
		</select>
		<br/>
		<label for="Scadenza-anno">Anno di scadenza:</label>
		<select name="Scadenza-anno" id="Scadenza-anno">
			<?php for ($i = date("Y"); $i <= date("Y") + 15; $i++): ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php endfor ?>
		</select>
		<br/>
	</div>
	<button id="pay" type="button">Termina e paga</button>
</form>
