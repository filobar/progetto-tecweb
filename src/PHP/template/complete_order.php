<h1>Riepilogo ordine</h1>

<?php $total = 0 ?>

<?php foreach ($dbh->getProductsByEmail($_SESSION["email"]) as $articolo):?>
	<article>
        <h2><?php echo str_replace(' ', '_', $articolo["Nome"]); ?></h2>
        <img src="../../img/shop/<?php echo $articolo["Tipo"] ?>/<?php echo str_replace(' ', '_', $articolo["Nome"]); ?>1.jpeg" alt="questo è un oggetto" />
        <div>
            <a href="product_index.php?id=<?php echo $articolo["ID_articolo"] ?>"><?php echo $articolo["Nome"] ?></a>
            <p id="quantity_<?php echo $articolo["ID_articolo"];?>">Quantità: <?php echo $articolo["Quantità"] ?></p>
            <p id="total_<?php echo $articolo["ID_articolo"];?>">€<?php 
                    $total = $total + number_format((float)($articolo["Prezzo"]*$articolo["Quantità"]), 2, '.', '');
                    echo number_format((float)($articolo["Prezzo"]*$articolo["Quantità"]), 2, '.', '');
                ?>
            </p>
        </div>
    </article>
<?php endforeach; ?>

<section>
    <h2>Riassunto ordine</h2>
    <p id=total_order>Totale ordine:€ <?php echo number_format((float)$total, 2, '.', '');?></p>
    <?php $user = $dbh->getUser($_SESSION["email"]); ?>
    <p>Indirizzo di spedizione</p>
    <p>Indirizzo: <?php if(empty($user["Indirizzo"])) echo "N/A"; 
            else echo $user["Indirizzo"]; ?></p>
    <p>Città: <?php if(empty($user["Città"])) echo "N/A"; 
            else echo $user["Città"]; ?></p>
    <p>Provincia: <?php if(empty($user["Provincia"])) echo "N/A"; 
            else echo $user["Provincia"]; ?></p>
    <p>Paese: <?php if(empty($user["Paese"])) echo "N/A"; 
            else echo $user["Paese"]; ?></p>
</section>

<?php if(!empty($user["Indirizzo"]) && !empty($user["Città"]) && !empty($user["Provincia"]) && !empty($user["Paese"])): ?>
    <p><button id="pay">Vai al pagamento</button></p>
<?php else: ?>
    <p id="error">L'indirizzo di spedizione non è completo, aggiorna i dati nel tuo profilo.</p>
<?php endif; ?>