<!DOCTYPE html>
<html lang="it">
    <head>
        <link rel="stylesheet" type="text/css" title="Santini stylesheet" href="../CSS/<?php echo $templateParams["name"];?>.css" />
        <?php
            if(!isset($_COOKIE["night"])):
                echo "<link rel=\"stylesheet\" type=\"text/css\" title=\"Santini stylesheet\" href=\"../CSS/base.css\" />";
            else:
                echo "<link rel=\"stylesheet\" type=\"text/css\" title=\"Santini stylesheet\" href=\"../CSS/baseNight.css\" />";
            endif;
        ?>
        <script src="../JQUERY/jquery-1.11.3.min.js"></script>
        <script src="../JQUERY/base.js"></script>
        <script src="../JQUERY/<?php echo $templateParams["name"];?>.js"></script>
        <meta charset="utf-8" name="viewport" content="width=device-width, initialscale=1.0">
        <title><?php echo $templateParams["titolo"]; ?></title>
    </head>
    <body>
        <header>
            <p>
                <a href="index.php"><img src="../../img/utils/minilogo.png" alt="i santini"/></a>
            </p>
            <aside>
                <button id="btn_menu">
                    <img src="../../img/utils/menu.png" alt="menu" />
                </button>
            </aside>
            <nav>
                <p>
                    <button id="profile_button">
                        <img src="../../img/utils/user.png" alt="go to profile"/>
                    </button>
                </p>
            </nav>
        </header>
        <div id="menu">
                <ul>
                    <li>
                        <a href="index.php">HOME</a>
                    </li>
                    <li>
                        <a href="band_index.php">LA BAND</a>
                    </li>
                    <li>
                        <a href="gallery_index.php">GALLERY</a>
                    </li>
                    <li>
                        <a href="shop_index.php">SHOP</a>
                    </li>
                    <li>    
                        <a href="concerts_index.php">CONCERTI</a>
                    </li>
                    <li>   
                        <a href="notifies_index.php" id="notifiche">NOTIFICHE</a>
                    </li>
                    <li>    
                        <a href="cart_index.php">CARRELLO</a>
                    </li>
                    <li>    
                        <?php
                            if(!isset($_COOKIE["night"])):
                                echo "<button id=\"nm_button\"><img src=\"../../img/utils/night.png\" alt=\"night mode\"/></button>";
                            else:
                                echo "<button id=\"nm_button\"><img src=\"../../img/utils/day.png\" alt=\"night mode\"/></button>";
                            endif;
                        ?>
                    </li>
                </ul>            
            </div>
        <main>
            <?php
                require($templateParams["page"]);
            ?>
        </main>
        <footer>
            <div>
                <p id="socials">
                    <a href="http://www.instagram.com/i_santini_/"><img src="../../img/utils/instagram.png" alt="instagram" /></a>
                    <a href="https://www.facebook.com/I-Santini-701365186593421"><img src="../../img/utils/facebook.png" alt="facebook" /></a>
                    <a href="https://open.spotify.com/artist/5KM5nPl0jtOP9o3Dmx2LHx?si=KI-HhSPbQmSZCEBL02-2UA"><img src="../../img/utils/spotify.png" alt="spotify" /></a>
                </p>
                <p id="last">I Santini</p> 
            </div>
        </footer>
    </body>
</html>