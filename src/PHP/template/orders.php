<?php
    $ordini = $dbh->getAllMyOrders($_SESSION["email"]);
    if (empty($ordini)):
        echo "<div id=\"noOrders\">";
        echo "<p>Non hai ordini al momento.</p>";
        echo "<p><button id=\"go_shop\">Vai allo shop</button></p>";
        echo "</div>";
    else: 
?>

<table>
    <thead>
        <tr>
            <th>ID ordine</th>
            <th>Data arrivo</th>
            <th>Totale</th>
            <th>Stato</th>
        </tr>
    </thead>
    <tbody>

<?php
    foreach ($ordini as $ordine) {
        echo "<tr>";
        echo "<td>" . $ordine["ID_ordine"] . "</td>";
        echo "<td>" . $ordine["Data_arrivo"] . "</td>";
        echo "<td>" . $ordine["Totale"] . "</td>";
        echo "<td>";
        switch ($ordine["Stato"]) {
            case 1:
                echo "In attesa di conferma";
                break;
            case 2:
                echo "Confermato";
                break;
            case 3:
                echo "Spedito";
                break;
            case 4:
                echo "Consegnato";
                break;
            default:
                echo "Questo non doveva succedere =(";
                break;
        }
        echo "</td>";
        
        echo "</tr>";
    }
?>
    </tbody>
</table>
<?php endif; ?>