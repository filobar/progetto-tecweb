<section>
    <h2>Modifica informazioni</h2>
    <div>
        <?php if(file_exists("../../img/profile/".$_SESSION["email"].".jpeg")): ?>
            <img src="../../img/profile/<?php echo $_SESSION["email"]?>.jpeg" alt="foto profilo" />
        <?php else: ?>
            <img src="../../img/fotobase.png" alt="foto profilo" />
        <?php endif; ?>
        <form id="up_form" action="updateProfileImage.php" method="post" enctype="multipart/form-data">
            <label for="image">Modifica foto: </label>
            <input id="image" type="file" name="image" required/>
        </form>
    </div>
    <form id="main_form" method="post" action="profile-mod_index.php">
        <table>
            <tbody>
                <tr>
                    <td><label for="name">Nome: </label></td>
                    <td><input id="name" name="name" type="text" value="<?php echo $dbh->getSingleInfo("Nome") ?>"/></td>
                </tr>
                <tr>
                    <td><label for="surname">Cognome: </label></td>
                    <td><input id="surname" name="surname" type="text" value="<?php echo $dbh->getSingleInfo("Cognome") ?>" /></td>
                </tr>
                <tr>
                    <td><label for="country">Paese: </label></td>
                    <td><input id="country" name="country" type="text" value="<?php echo $dbh->getSingleInfo("Paese") ?>" /></td>
                </tr>
                <tr>
                    <td><label for="province">Provincia: </label></td>
                    <td><input id="province" name="province" type="text" value="<?php echo $dbh->getSingleInfo("Provincia") ?>" /></td>
                </tr>
                <tr>
                    <td><label for="city">Città: </label></td>
                    <td><input id="city" name="city" type="text" value="<?php echo $dbh->getSingleInfo("Città") ?>" /></td>
                </tr>
                <tr>
                    <td><label for="address">Indirizzo: </label></td>
                    <td><input id="address" name="address" type="text" value="<?php echo $dbh->getSingleInfo("Indirizzo") ?>" /></td>
                </tr>
            </tbody>
        </table>
        <button type="submit" id="modify">Conferma modifiche</button>
    </form>
</section>

<?php
    if( !empty($_POST["name"]) &&
        !empty($_POST["surname"])):

        $dbh->updateUser($_POST["name"],
                         $_POST["surname"],
                         $_POST["country"],
                         $_POST["province"],
                         $_POST["city"],
                         $_POST["address"]);

        header("Location: profile_index.php");
        die();
    elseif(isset($_POST["name"]) || isset($_POST["surname"])):
        echo "<p>Molto male, devi inserire almeno nome e cognome</p>";
    endif;
?>