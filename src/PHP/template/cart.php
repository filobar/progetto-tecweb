<?php $total = 0; ?>

<?php foreach ($dbh->getProductsByEmail($_SESSION["email"]) as $articolo):?>

    <article>
        <h3>Articolo</h3>
        <img src="../../img/shop/<?php echo $articolo["Tipo"] ?>/<?php echo str_replace(' ', '_', $articolo["Nome"]); ?>1.jpeg" alt="immagine <?php echo $articolo["Nome"]; ?>" />
        <div>
            <a href="product_index.php?id=<?php echo $articolo["ID_articolo"] ?>"><?php echo $articolo["Nome"] ?></a>
            <p id="quantity_<?php echo $articolo["ID_articolo"];?>">Quantità: <?php echo $articolo["Quantità"] ?></p>
            <p id="total_<?php echo $articolo["ID_articolo"];?>">€<?php 
                    $total = $total + number_format((float)($articolo["Prezzo"]*$articolo["Quantità"]), 2, '.', '');
                    echo number_format((float)($articolo["Prezzo"]*$articolo["Quantità"]), 2, '.', '');
                ?>
            </p>
            <p>
                <button class="increment" type="button">+</button>
                <button class="decrement" type="button">-</button>
            </p>
        </div>
    </article>

<?php endforeach; ?>

<footer>

    <?php
        if ($dbh->isCartEmpty()) {
            echo "<p>Il tuo carrello è vuoto</p>";
            echo "<p><button id=\"go_shop\">Vai allo shop</button></p>";
        }
    ?>

    <?php
        if (!$dbh->isCartEmpty()) {

            // Aggiungo l data di arrivo prevista
            $datetime = date("d/m/Y", strtotime("+15 day"));
            echo "<p>Data di arrivo prevista: " . $datetime . "</p>";

            // Aggiungo il totale dell'ordine
            echo "<p id=\"total_order\">Totale ordine: €" . number_format((float)$total, 2, '.', '') . "</p>";

            // Aggiungo il pulsante per pagare l'ordine
            echo "<p><button id=\"complete_order\" type=\"button\">Completa l'ordine</button></p>";

            // Aggiungo il pulsante per svuotare il carrello
            echo "<p><button id=\"empty_cart\" type=\"button\">Svuota carrello</button></p>";
        }
    ?>

</footer>