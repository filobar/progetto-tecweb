<h1>
    Notifiche
</h1>

<?php $dbh->updateCheck() ?>

<?php $notifies = $dbh->getNotifications($_SESSION["email"]);
      $dbh->viewNotifications($_SESSION["email"]);
      if(empty($notifies)):
          echo "<p>Non ci sono notifiche per te.</p>";
      else: ?>

<?php   foreach ($notifies as $notifica):?>
            <article class=<?php echo $notifica["Visualizzata"]?"visualized":"not-visualized"; ?>>
                <h2 class="titolo"><?php echo $notifica["Titolo"];?></h2>
                <p class="descrizione"><?php echo $notifica["Descrizione"];?>
                <p class="data"><?php echo $notifica["Data_creazione"];?>
            </article>
        <?php endforeach; ?>

<?php endif; ?>