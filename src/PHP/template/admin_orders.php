<?php
    $ordini = $dbh->getAllOrders();
    if (empty($ordini)):
        echo "<p id=\"noOrders\">Non ci sono ordini.</p>";
    else: 
?>

<table>
    <thead>
        <tr>
            <th>Email acquirente</th>
            <th>ID Ordine</th>
            <th>Stato</th>
            <th>Cambia stato</th>
            <th>Vedi ordine</th>
        </tr>
    </thead>
    <tbody>

<?php
    $stati = array(
        1 => "In attesa di conferma",
        2 => "Confermato",
        3 => "Spedito",
        4 => "Consegnato"
    );
    foreach ($ordini as $ordine) {
        echo "<tr>";

        //echo "<td>" . $ordine["Data_arrivo"] . "</td>";
        //echo "<td>" . $ordine["Totale"] . "</td>";
        echo "<td>" . $ordine["Email"] . "</td>";
        echo "<td>" . $ordine["ID_ordine"] . "</td>";

        echo "<td>";
        if (array_key_exists($ordine["Stato"], $stati)) {
            echo $stati[$ordine["Stato"]];
        }
        else {
            echo "Questo non doveva succedere =(";
            var_dump($ordine);
        }
        echo "</td>";
        
        if ($ordine["Stato"] < 4) {
            echo "<td><button id=\"change_" . $ordine["ID_ordine"] . "\">Passa a \"" .$stati[$ordine["Stato"]+1]. "\"</button></td>";
        }
        else {
            echo "<td></td>";
        }

        echo "<td><button id=\"" . $ordine["ID_ordine"] . "\">Vedi ordine</button></td>";

        echo "</tr>";
    }
?>
    </tbody>
</table>
<?php endif; ?>