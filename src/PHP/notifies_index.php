<?php
    require_once("bootstrap.php");

    if (isUserLoggedIn()) {
	    $templateParams["titolo"] = "I Santini - Notifiche";
	    $templateParams["page"] = "notifies.php";
	    $templateParams["name"] = "notifies";
	    require("template/base.php");
	}
	else {
		header("Location: login_index.php");
		die();
	}
?>