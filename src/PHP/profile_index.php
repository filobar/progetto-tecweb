<?php
    require_once("bootstrap.php");

    if(isUserLoggedIn()):
        $templateParams["titolo"] = "I Santini - Profilo";
        $templateParams["page"] = "profile.php";
        $templateParams["name"] = "profile";
        require("template/base.php");
    else:
        header("Location: login_index.php");
        die();
    endif;
    
    //se l'utente è loggato apro il profilo, altrimenti lo porto a login_index
    
?>