<?php
    require_once("bootstrap.php");

    $templateParams["titolo"] = "I Santini - Pagamento avvenuto con successo";
    $templateParams["page"] = "order_ordered.php";
    $templateParams["name"] = "order_ordered";

    require("template/base.php");
?>