<?php
    class DatabaseHelper{
        private $db;

        public function __construct($servername, $username, $password, $dbname){
            $this->db = new mysqli($servername, $username, $password, $dbname);
            if ($this->db->connect_error) {
                die("Connection failed: " . $db->connect_error);
            }
        }

        public function getCategories(){
            $stmt = $this->db->prepare("SELECT DISTINCT Tipo FROM ARTICOLO");
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getProducts($sort){
            if ($sort == "alf") $sorting = "Nome ASC";
            else if ($sort == "pr_asc") $sorting = "Prezzo ASC";
            else if ($sort == "pr_desc") $sorting = "Prezzo DESC";
            else if ($sort == "disp") $sorting = "Numero_rimasti DESC";
            else $sorting = "Nome ASC";

            $stmt = $this->db->prepare("SELECT * FROM ARTICOLO WHERE Numero_rimasti>0 ORDER BY " . $sorting);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getProductsByEmail($email){
            $stmt = $this->db->prepare("SELECT * FROM carrello C, articolo A WHERE Email = ? AND A.ID_articolo = C.ID_articolo");
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getCartByEmail($email){
            $stmt = $this->db->prepare("SELECT * FROM carrello WHERE Email = ?");
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getProduct($id){
            $stmt = $this->db->prepare("SELECT * FROM ARTICOLO WHERE ID_articolo = $id");
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function addProduct($nome, $categoria, $prezzo, $quantita){
            $stmt = $this->db->prepare("INSERT INTO articolo (Nome, Tipo, Prezzo, Numero_rimasti) VALUES (?,?,?,?);");
            $stmt->bind_param('ssss', $nome, $categoria, $prezzo, $quantita);
            $stmt->execute();
        }

        public function isProductInCart($email, $id_articolo){
            $query = "SELECT Quantità FROM carrello WHERE `Email` = ? AND `ID_articolo` = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ss', $email, $id_articolo);
            $stmt->execute();
            $result = $stmt->get_result();

            $result->fetch_all(MYSQLI_ASSOC);

            foreach ($result as $r):
                return $r["Quantità"];
            endforeach;

            return 0;
        }

        public function addIntoCart($email, $id_articolo, $quantita){
            $q = $this->isProductInCart($email, $id_articolo);
            if($q!=0):
                $stmt = $this->db->prepare("UPDATE carrello SET Quantità = ? + $q WHERE Email = ? AND ID_articolo = ?");
                $stmt->bind_param('iss', $quantita, $email, $id_articolo);
            else:
                $stmt = $this->db->prepare("INSERT INTO carrello VALUES(?, ?, ?)");
                $stmt->bind_param('ssi', $id_articolo, $email, $quantita);
            endif;
            $stmt->execute();
        }

        // Restituisce i dati corrispondenti all'email immessa
        public function checkLogin($email, $password){
            $query = "SELECT Email FROM utente WHERE `Email` = ? AND `Password` = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ss', $email, $password);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        // Restituisce true o false se esiste un utente già registrato con la mail immessa
        public function checkEmail($email){
            $query = "SELECT Email FROM utente WHERE `Email` = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return !empty($result->fetch_all(MYSQLI_ASSOC));
        }

        public function getSingleInfo($info){
            $query = "SELECT $info FROM utente WHERE Email = '{$_SESSION['email']}'";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC)[0]["$info"];
        }

        public function getUser($email){
            $query = "SELECT * FROM utente WHERE Email=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC)[0];
        }

        public function registerNewUser($name, $surname, $email, $password, $country, $province, $city, $address) {
            if ($this->checkEmail($email)) {
                return false;
            }
            else {
                $query = "INSERT INTO utente VALUES(?,?,?,?,?,?,?,?,?)";
                $stmt = $this->db->prepare($query);
                $curr_date = date('Y-m-d H:i:s');
                $stmt->bind_param('sssssssss', $name, $surname, $email, $password, $country, $province, $city, $address, $curr_date);
                $stmt->execute();
                return true;
            }
        }

        public function updateUser($name, $surname, $country, $province, $city, $address) {
            $query = "UPDATE utente SET Nome = ?, Cognome = ?, Paese = ?, Provincia = ?, Città = ?, Indirizzo = ? WHERE Email = '{$_SESSION['email']}'";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ssssss', $name, $surname, $country, $province, $city, $address);
            $stmt->execute();
        }

        public function updateQuantity($product_id, $new_quantity) {
            $email = $_SESSION["email"];
            if ($new_quantity == 0) {
                // Cancello il prodotto
                $query = "DELETE FROM carrello WHERE ID_articolo=? AND Email=?";
                $stmt = $this->db->prepare($query);
                $stmt->bind_param("ss", $product_id, $email);
                $stmt->execute();
            }
            else {
                // Aggiorno la quantità
                $query = "UPDATE carrello SET Quantità=? WHERE ID_articolo=? AND Email=?";
                $stmt = $this->db->prepare($query);
                $stmt->bind_param("sss", $new_quantity, $product_id, $email);
                $stmt->execute();
            }
        }

        public function emptyCart() {
            $email = $_SESSION["email"];

            $query = "DELETE FROM carrello WHERE Email=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("s", $email);
            $stmt->execute();
        }

        public function isCartEmpty() {
            $email = $_SESSION["email"];

            $query = "SELECT * FROM carrello WHERE Email=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return empty($result->fetch_all(MYSQLI_ASSOC));
        }

        public function getLastInsertID(){
            $stmt = $this->db->prepare("SELECT LAST_INSERT_ID() as ID");
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC)[0]["ID"];
        }

        public function addConcert($data, $ora, $nome, $luogo){
            $numero = $this->getLastInsertID();
            $stmt = $this->db->prepare("INSERT INTO concerto (Codice_biglietto, Nome, Luogo, `Data`, Ora) VALUES (?,?,?,?,?);");
            $stmt->bind_param('issss', $numero, $nome, $luogo, $data, $ora);
            $stmt->execute();
        }

        public function getAllConcerts() {
            $query = "SELECT C.Codice_biglietto, C.Nome, C.Luogo, C.Data, C.Ora, A.Numero_rimasti FROM concerto C, articolo A WHERE C.Codice_biglietto = A.ID_articolo AND A.Numero_rimasti>0";

            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getNotifications($email) {
            $query = "SELECT * FROM notifica WHERE Email = ? ORDER BY Visualizzata ASC";
            
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getAllOrders() {
            $query = "SELECT * FROM ordine";

            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getAllMyOrders($email) {
            $query = "SELECT * FROM ordine WHERE Email = ?";
            
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function getOrder($id_ordine) {
            $stmt = $this->db->prepare("SELECT * FROM ordine O, articolo_in_ordine AIO, articolo A WHERE O.ID_ordine = ? AND O.ID_ordine = AIO.ID_ordine AND AIO.ID_articolo = A.ID_articolo");

            $stmt->bind_param('s', $id_ordine);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function checkOrder(){
            // Controllo che le quantità siano corrette.
            $cart = $this->getCartByEmail($_SESSION["email"]);
            foreach ($cart as $article_in_cart) {
                $query = "SELECT (A.Numero_rimasti-C.Quantità) as diff FROM articolo A, carrello C WHERE A.ID_articolo=C.ID_articolo AND A.ID_articolo=? AND C.Email = ?";
                $stmt = $this->db->prepare($query);
                $stmt->bind_param('ss', $article_in_cart["ID_articolo"], $_SESSION["email"]);
                $stmt->execute();
                $result = $stmt->get_result();
                if (intval($result->fetch_all(MYSQLI_ASSOC)[0]["diff"]) < 0) return false;
            }
            return true;
        }

        public function createOrder() {
            // Creo il nuovo ordine
            $stmti = $this->db->prepare("INSERT INTO ordine (Data_arrivo, Totale, Email, Stato) VALUES (?, 
                (SELECT SUM(Quantità * Prezzo) FROM carrello c, articolo a WHERE c.ID_articolo = a.ID_articolo AND c.Email=?), ?, 1)");
            $stmti->bind_param('sss', $_SESSION["DATA_ARRIVO"], $_SESSION["email"], $_SESSION["email"]);
            $stmti->execute();

            // Prendo il nuovo id dell'ordine appena inserito
            return $this->getLastInsertID();
        }

        public function fillOrder($idOrd) {
            $cart = $this->getCartByEmail($_SESSION["email"]);
            $dt = date("Y-m-d H:i:s");

            // Per ogni articolo nel carrello
            foreach ($cart as $article):
                // Lo aggiungo all'ordine
                $stmt = $this->db->prepare("INSERT INTO articolo_in_ordine VALUES (?, ?, ?)");
                $stmt->bind_param('ssi', $idOrd, $article["ID_articolo"], $article["Quantità"]);
                $stmt->execute();

                // Prendo la vecchia quantità dell'articolo
                $stmt = $this->db->prepare("SELECT Numero_rimasti FROM articolo WHERE ID_articolo=?");
                $stmt->bind_param('s', $article["ID_articolo"]);
                $stmt->execute();
                $result = $stmt->get_result();
                $new_quantity = $result->fetch_all(MYSQLI_ASSOC)[0]["Numero_rimasti"] - $article["Quantità"];

                // Aggiorno le quantità degli articoli
                $stmt = $this->db->prepare("UPDATE articolo SET Numero_rimasti=? WHERE ID_articolo=?");
                $stmt->bind_param('is', $new_quantity, $article["ID_articolo"]);
                $stmt->execute();

                // E notifico l'admin (se gli articoli disponibili sono <=10)
                if ($new_quantity <= 10) {
                    $query = "INSERT INTO notifica(Visualizzata, Titolo, Descrizione, Email, Data_creazione) VALUES (false,\"Articolo quasi terminato\",\"L'articolo con ID ".$article["ID_articolo"]." sta terminando\",\"admin@tecweb.it\",\"".$dt."\")";
                    $stmt = $this->db->prepare($query);
                    $stmt->execute();
                }
            endforeach;
        }

        public function areThereNotifies() {
            $stmt = $this->db->prepare("SELECT COUNT(*) AS cnt FROM notifica WHERE Email = ? AND Data_creazione >= ?");
            $lastC = $this->lastCheck();
            $stmt->bind_param('ss', $_SESSION["email"], $lastC);
            $stmt->execute();

            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC)[0]["cnt"];
        }

        public function lastCheck() {
            if (isUserLoggedIn()) {
                $stmt = $this->db->prepare("SELECT Ultimo_controllo AS uc FROM utente WHERE  Email=?");
                $stmt->bind_param('s', $_SESSION["email"]);
                $stmt->execute();

                $result = $stmt->get_result();

                return $result->fetch_all(MYSQLI_ASSOC)[0]["uc"];
            }
            else return 0;
        }

        public function updateCheck() {
            $stmt = $this->db->prepare("UPDATE utente SET Ultimo_controllo = ? WHERE Email = ?");
            $dt = date("Y-m-d H:i:s");
            $stmt->bind_param('ss', $dt, $_SESSION["email"]);
            $stmt->execute();
        }

        public function getOrderState($id) {
            $stmt = $this->db->prepare("SELECT Stato FROM ordine WHERE ID_ordine=" . $id);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC)[0]["Stato"];
        }

        public function updateOrderState($id, $new_state) {
            $query = "UPDATE ordine SET Stato=? WHERE ID_ordine=" . $id;
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $new_state);
            $stmt->execute();

            switch ($new_state) {
                case 1:
                    $stato = "in attesa di conferma";
                    break;
                case 2:
                    $stato = "confermato";
                    break;
                case 3:
                    $stato = "spedito";
                    break;
                case 4:
                    $stato = "consegnato";
                    break;
                default:
                    $stato = "rotto come questo sito =(";
                    break;
            }

            $this->createNotification("Il tuo ordine è stato ".$stato, "L'ordine con ID ".$id." ora è ".$stato, $id);
        }

        public function createNotification($titolo, $descrizione, $id_ordine) {
            // Ricavo l'email dell'utente dall'id ordine
            $stmt = $this->db->prepare("SELECT Email FROM ordine WHERE ID_ordine=?");
            $stmt->bind_param('i', $id_ordine);
            $stmt->execute();
            $result = $stmt->get_result();
            $email = $result->fetch_all(MYSQLI_ASSOC)[0]["Email"];

            // Preparo la data corrente
            $dt = date("Y-m-d H:i:s");

            $query = "INSERT INTO notifica (Visualizzata, Titolo, Descrizione, Email, ID_ordine, Data_creazione) VALUES (false, ?, ?, ?, ?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('sssis', $titolo, $descrizione, $email, $id_ordine, $dt);
            $stmt->execute();
        }

        // Sets all not-yet-visualized notification of the given user to "visualized"
        public function viewNotifications($email) {
            $query = "UPDATE notifica SET Visualizzata=1 WHERE Email=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
        }

        public function deleteArticle($id_articolo) {
            $query = "UPDATE articolo SET Numero_rimasti=0 WHERE ID_articolo=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $id_articolo);
            $stmt->execute();
        }

        public function deleteConcert($codice_biglietto) {
            $this->deleteArticle($codice_biglietto);
            $query = "DELETE FROM concerto WHERE Codice_biglietto=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $codice_biglietto);
            $stmt->execute();
        }

        public function getHomeArticles() {
            $stmt = $this->db->prepare("SELECT * FROM ARTICOLO_HOME");
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function modifyProduct($id_articolo, $quantity, $price) {
            $stmt = $this->db->prepare("UPDATE articolo SET Numero_rimasti=?, Prezzo=? WHERE ID_articolo=?");
            $stmt->bind_param('sss', $quantity, $price, $id_articolo);
            $stmt->execute();
        }

        public function updateHomeArticle($title, $content, $id) {
            $stmt = $this->db->prepare("UPDATE ARTICOLO_HOME SET Titolo=?, Testo=? WHERE ID_articolo=?");
            $stmt->bind_param('ssi', $title, $content, $id);
            $stmt->execute();
        }

        public function deleteHomeArticle($id) {
            $stmt = $this->db->prepare("DELETE FROM ARTICOLO_HOME WHERE ID_articolo=?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
        }

        public function addHomeArticle($title, $content) {
            $stmt = $this->db->prepare("INSERT INTO ARTICOLO_HOME(Titolo, Testo) VALUES (?, ?)");
            $stmt->bind_param('ss', $title, $content);
            $stmt->execute();
        }
    }
?>