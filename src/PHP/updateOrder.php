<?php
    require_once("bootstrap.php");
    
    // Prendo lo stato corrente dell'ordine
    $stato = $dbh->getOrderState($_GET["id"]);

    if ($stato < 4) {
        // Aggiorno lo stato dell'ordine
        $stato = $stato + 1;

        $dbh->updateOrderState($_GET["id"], $stato);
    }
    
?>